package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.TreeMap;

public class PN extends Ordination {

	public double antalEnheder;
	public TreeMap<LocalDate, Double> doser = new TreeMap<>();
	public int dosisAnvendt = 0;

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	// En sygeplejerske kan komme til at glemme at registrere, at givDosis bliver
	// givet i rækkefølge.
	//

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return dosisGivet
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean dosisGivet = false;
		if (givesDen.equals(getStartDen()) || givesDen.isBefore(super.getSlutDen()) && (givesDen.isAfter(super.getStartDen()) || givesDen.equals(getSlutDen()))) {
			if (doser.containsKey(givesDen)) {
				doser.put(givesDen, doser.get(givesDen).doubleValue() + antalEnheder);
			} else {
				doser.put(givesDen, antalEnheder);
			}
			dosisGivet = true;
			dosisAnvendt++;
		}
		return dosisGivet;
	}

	/**
	 * Beregner en patients døgnDosis ud fra samletDosis() og antalDage hvor
	 * antalDage er antal dage mellem første givet Dosis og sidste givet Dosis.
	 *
	 */
	@Override
	public double doegnDosis() {
		LocalDate high = null;
		LocalDate low = null;
		int antalDage = 0;
		if (!doser.isEmpty()) {
			high = doser.lastKey();
			low = doser.firstKey();
			antalDage = (int) ChronoUnit.DAYS.between(low, high) + 1;
		}
		return samletDosis() / antalDage;
	}

	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * getAntalEnheder();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return dosisAnvendt;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
