package ordination;

import java.time.*;

public class DagligFast extends Ordination {


	private double morgenAntal;
	private double middagAntal;
	private double aftenAntal;
	private double natAntal;

	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemiddel);
		doser[0] = createDosis(LocalTime.parse("07:00"), morgenAntal);
		doser[1] = createDosis(LocalTime.parse("12:00"), middagAntal);
		doser[2] = createDosis(LocalTime.parse("18:00"), aftenAntal);
		doser[3] = createDosis(LocalTime.parse("23:00"), natAntal);
		this.morgenAntal = morgenAntal;
		this.middagAntal = middagAntal;
		this.aftenAntal = aftenAntal;
		this.natAntal = natAntal;
	}

	public double getMorgenAntal() {
		return morgenAntal;
	}

	public void setMorgenAntal(double morgenAntal) {
		this.morgenAntal = morgenAntal;
	}

	public double getMiddagAntal() {
		return middagAntal;
	}

	public void setMiddagAntal(double middagAntal) {
		this.middagAntal = middagAntal;
	}

	public double getAftenAntal() {
		return aftenAntal;
	}

	public void setAftenAntal(double aftenAntal) {
		this.aftenAntal = aftenAntal;
	}

	public double getNatAntal() {
		return natAntal;
	}

	public void setNatAntal(double natAntal) {
		this.natAntal = natAntal;
	}

	@Override
	public double samletDosis() {
		return (morgenAntal + middagAntal + aftenAntal + natAntal) * super.antalDage();
	}

	@Override
	public double doegnDosis() {
		return morgenAntal + middagAntal + aftenAntal + natAntal;

	}
	// Opretter ny dosis med specificieret tid på dagen
	public Dosis createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		return dosis;
	}

	public Dosis[] getDoser() {
		return doser;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

}
