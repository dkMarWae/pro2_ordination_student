package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	// Array af antal Dosis en patient skal have i løbet af dagen.
	public ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
		// TODO Auto-generated constructor stub
	}
	/*
	 * Opretter en Dosis med parametrene LocalTime tid og double antal.
	 * Tilføjer Dosis til en ArrayList af <Dosis> hvis den oprettede Dosis ikke allerede eksistere i ArrayListen.
	 */
	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		if (!doser.contains(dosis)) {
			doser.add(dosis);
		}
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double doegnDosis = 0.0;
		for (Dosis d : doser) {
			doegnDosis += d.getAntal();
		}
		return doegnDosis;
	}

	@Override
	public String getType() {
		return "Daglig Skæv";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(doser);
	}
}
