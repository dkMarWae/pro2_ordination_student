package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import controller.Controller;

public class ControllerTest {
	
	private Patient patient;
	private Laegemiddel laegemiddel;
	private LocalDate startDen;
	private LocalDate slutDen;
	
	@Before
	public void setUp() throws Exception {
		patient = new Patient("12346-7890", "Martin Wänglund", 70);
		laegemiddel = new Laegemiddel("Fucidin", 1, 2, 3, "Styk");
		startDen = LocalDate.of(2021, 3, 8);
		slutDen = LocalDate.of(2021, 3, 10);
		
	}

	@Test
	public void opretPNOridnationTestTC1() {
		PN pn = Controller.getController().opretPNOrdination(startDen, slutDen,patient, laegemiddel, 2);
		patient.addOrdination(pn);
		
		assertEquals(2, pn.getAntalEnheder(), 0.00);
		assertEquals(LocalDate.of(2021, 3, 8), pn.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 10), pn.getSlutDen());
		assertEquals("Fucidin", pn.getLaegemiddel().toString());
		assertNotNull(patient.getOrdinationer());
	}	
	
	@Test
	public void opretPNOridnationTestTC2() {
		try {
			PN pn = Controller.getController().opretPNOrdination(slutDen, startDen,patient, laegemiddel, 2);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Start dato er efter slut dato");
		}
	}
		
	@Test
	public void opretDagligFastOridnationTestTC1() {
		DagligFast df = Controller.getController()
		.opretDagligFastOrdination(startDen, slutDen, patient, laegemiddel, 2,2,2,2);
		patient.addOrdination(df);
		
		Dosis[] expectedArray = new Dosis[4];
		expectedArray[0] = new Dosis(LocalTime.of(07, 00), 2);
		expectedArray[1] = new Dosis(LocalTime.of(12, 00), 2);
		expectedArray[2] = new Dosis(LocalTime.of(18, 00), 2);
		expectedArray[3] = new Dosis(LocalTime.of(23, 00), 2);
		
		Dosis[] actualArray = df.getDoser();
		
		assertEquals(LocalDate.of(2021, 3, 8), df.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 10), df.getSlutDen());
		assertEquals("Fucidin", df.getLaegemiddel().toString());
		assertEquals(Arrays.toString(expectedArray), Arrays.toString(actualArray));
		assertNotNull(patient.getOrdinationer());
		assertEquals(2, df.getMorgenAntal(), 0.00);
		assertEquals(2, df.getMiddagAntal(), 0.00);
		assertEquals(2, df.getAftenAntal(), 0.00);
		assertEquals(2, df.getNatAntal(), 0.00);	
	}
	
	@Test
	public void opretDagligFastOridnationTestTC2() {
		try {
			DagligFast df = Controller.getController()
			.opretDagligFastOrdination(slutDen, startDen, patient, laegemiddel, 2,2,2,2);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Start dato er efter slut dato");
		}
	}
	
	@Test
	public void opretDagligSkaevOridnationTestTC1() {
		LocalTime[] klokkeSlet = new LocalTime[3];
		klokkeSlet[0] = LocalTime.of(10, 30);
		klokkeSlet[1] = LocalTime.of(13, 00);
		klokkeSlet[2] = LocalTime.of(18, 30);
		double[] antalEnheder = new double[3];
		antalEnheder[0] = 2;
		antalEnheder[1] = 1;
		antalEnheder[2] = 3;
		
		DagligSkaev ds = Controller.getController()
		.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antalEnheder);
		patient.addOrdination(ds);
		
		assertEquals(LocalDate.of(2021, 3, 8), ds.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 10), ds.getSlutDen());
		assertEquals("Fucidin", ds.getLaegemiddel().toString());
		assertNotNull(patient.getOrdinationer());
		assertEquals(klokkeSlet[0], ds.getDoser().get(0).getTid());
		assertEquals(antalEnheder[0], ds.getDoser().get(0).getAntal(), 0.00);
	}
		
	@Test
	public void opretDagligSkaevOridnationTestTC2() {
		LocalTime[] klokkeSlet = new LocalTime[3];
		klokkeSlet[0] = LocalTime.of(10, 30);
		klokkeSlet[1] = LocalTime.of(13, 00);
		klokkeSlet[2] = LocalTime.of(18, 30);
		double[] antalEnheder = new double[3];
		antalEnheder[0] = 2;
		antalEnheder[1] = 1;
		antalEnheder[2] = 3;
		try {
			DagligSkaev ds = Controller.getController()
			.opretDagligSkaevOrdination(slutDen, startDen, patient, laegemiddel, klokkeSlet, antalEnheder);		
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Start dato er efter slut dato");
		}
	}
		
	@Test
	public void opretDagligSkaevOridnationTestTC3() {
		LocalTime[] klokkeSlet = new LocalTime[4];
		klokkeSlet[0] = LocalTime.of(10, 30);
		klokkeSlet[1] = LocalTime.of(13, 00);
		klokkeSlet[2] = LocalTime.of(18, 30);
		klokkeSlet[3] = LocalTime.of(21, 00);
		double[] antalEnheder = new double[3];
		antalEnheder[0] = 2;
		antalEnheder[1] = 1;
		antalEnheder[2] = 3;
		try {
			DagligSkaev ds = Controller.getController()
			.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antalEnheder);		
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Der skete en fejl");
		}
	}
	
	@Test
	public void ordinationPNAnvnedtTestTC1() {
		PN pn = Controller.getController().opretPNOrdination(startDen, slutDen,patient, laegemiddel, 2);
		patient.addOrdination(pn);
		pn.givDosis(LocalDate.of(2021, 03, 9));
		
		assertEquals(1, pn.getAntalGangeGivet());
	}
		
	@Test
	public void ordinationPNAnvnedtTestTC2() {
		PN pn = Controller.getController().opretPNOrdination(startDen, slutDen,patient, laegemiddel, 2);
		patient.addOrdination(pn);
		try {
			Controller.getController().ordinationPNAnvendt(pn, LocalDate.of(2021, 03, 7));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Valgte dato er udenfor Ordinations perioden");
		}
	}
	
	@Test
	public void anbefaletDosisPrDoegnTest() {
		patient.setVaegt(24);
		assertEquals(24, Controller.getController().anbefaletDosisPrDoegn(patient, laegemiddel), 0.1);
		patient.setVaegt(25);
		assertEquals(50, Controller.getController().anbefaletDosisPrDoegn(patient, laegemiddel), 0.1);
		patient.setVaegt(70);
		assertEquals(140, Controller.getController().anbefaletDosisPrDoegn(patient, laegemiddel), 0.1);
		patient.setVaegt(120);
		assertEquals(240, Controller.getController().anbefaletDosisPrDoegn(patient, laegemiddel), 0.1);
		patient.setVaegt(121);
		assertEquals(363, Controller.getController().anbefaletDosisPrDoegn(patient, laegemiddel), 0.1);
	}
	
	@Test
	public void antalOrdinationerPrVægtPrLægemiddelTest() {
		assertEquals(1, Controller.getController().antalOrdinationerPrVægtPrLægemiddel(1, 24, laegemiddel), 0.1);
		assertEquals(2, Controller.getController().antalOrdinationerPrVægtPrLægemiddel(25, 50, laegemiddel), 0.1);
		assertEquals(2, Controller.getController().antalOrdinationerPrVægtPrLægemiddel(30, 110, laegemiddel), 0.1);
		assertEquals(2, Controller.getController().antalOrdinationerPrVægtPrLægemiddel(50, 120, laegemiddel), 0.1);
		assertEquals(3, Controller.getController().antalOrdinationerPrVægtPrLægemiddel(119, 125, laegemiddel), 0.1);
	}

}
