package test;

import static org.junit.Assert.*;

import ordination.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

public class DagligFastTest {
	LocalDate startDate;
	LocalDate endDate;
	Laegemiddel lmSertralin;
	Patient pt;

	@Before
	public void setUp() throws Exception {
		startDate = LocalDate.parse("2021-03-08");
		endDate = LocalDate.parse("2021-03-10");
		lmSertralin = new Laegemiddel("Sertralin", 5, 20, 30, "stk");
		pt = new Patient("1234567-890", "Martin Wänglund", 70);
	}
	
	@Test
	public void testSamletDosis() {
		DagligFast df = new DagligFast(startDate, endDate, lmSertralin, 2, 2, 2, 2);
		assertNotNull(df);
		pt.addOrdination(df);
		assertEquals(df.samletDosis(), 24, 0);
	}
	
	@Test
	public void testDoegnDosis() {
		DagligFast df = new DagligFast(startDate, endDate, lmSertralin, 2, 2, 2, 2);
		assertNotNull(df);
		pt.addOrdination(df);
		assertEquals(df.doegnDosis(), 8, 0);
	}
	
	@Test
	public void testCreateDosisTC1() {
		DagligFast dfTestData = new DagligFast
		(LocalDate.parse("2021-03-08"), LocalDate.parse("2021-03-10"),lmSertralin, 0,2,2,2);
		//System.out.println(Arrays.toString(dfTestData.getDoser()));
		
		DagligFast df = new DagligFast(startDate, endDate, lmSertralin, 0, 2, 2, 2);
		//System.out.println(Arrays.toString(df.getDoser()));
		assertArrayEquals(df.getDoser(), df.getDoser());
	}
	
	@Test
	public void testCreateDosisTC2() {
		DagligFast dfTestData = new DagligFast
		(LocalDate.parse("2021-03-08"), LocalDate.parse("2021-03-10"),lmSertralin, 1,2,2,2);
		//System.out.println(Arrays.toString(dfTestData.getDoser()));
		
		DagligFast df = new DagligFast(startDate, endDate, lmSertralin, 1, 2, 2, 2);
		//System.out.println(Arrays.toString(df.getDoser()));
		assertArrayEquals(df.getDoser(), df.getDoser());
	}

}
