package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.*;

public class PNTest_final {
	LocalDate d1;
	LocalDate d2;
	LocalDate d3;
	LocalDate startDate;
	LocalDate stopDate;
	LocalDate ordinationsDato1;
	LocalDate ordinationsDato2;
	LocalDate ordinationsDato3;
	Laegemiddel testLM;
	PN pn;

	@Before
	public void setUp() throws Exception {
		d1 = LocalDate.parse("2021-03-08");
		d2 = LocalDate.parse("2021-03-06");
		d3 = LocalDate.parse("2021-03-14");
		startDate = LocalDate.parse("2021-03-07");
		stopDate = LocalDate.parse("2021-03-13");
		testLM = new Laegemiddel("Cebocap Adjuvant", 200, 400, 800, "mg");
		
		ordinationsDato1 = LocalDate.parse("2021-03-07");
		ordinationsDato2 = LocalDate.parse("2021-03-09");
		ordinationsDato3 = LocalDate.parse("2021-03-11");
		
	}

	@Test
	public void testSamletDosis() {
		pn = new PN(startDate, stopDate, testLM, 2);
		assertNotNull(pn);
		
		pn.givDosis(ordinationsDato1);
		pn.givDosis(ordinationsDato2);
		pn.givDosis(ordinationsDato2);
		pn.givDosis(ordinationsDato2);
		pn.givDosis(ordinationsDato3);
		
		int outcome = pn.getAntalGangeGivet();
		
		assertEquals(outcome, 5);
	}

	@Test
	public void testDoegnDosis_TC1() {
		pn = new PN(startDate, stopDate, testLM, 2);
		assertNotNull(pn);

		pn.givDosis(ordinationsDato1);
		pn.givDosis(ordinationsDato2);
		pn.givDosis(ordinationsDato2);
		pn.givDosis(ordinationsDato2);
		pn.givDosis(ordinationsDato3);
		
		double outcome = pn.doegnDosis();
		
		assertEquals(outcome, 2.6666666, 1);
	}

	@Test
	public void testGivDosis_TC1() {
		pn = new PN(startDate, stopDate, testLM, 2);
		assertNotNull(pn);
		
		boolean outcome = pn.givDosis(d1);
		assertTrue(outcome);
	}
	
	@Test
	public void testGivDosis_TC2() {
		pn = new PN(startDate, stopDate, testLM, 2);
		assertNotNull(pn);
		boolean outcome = pn.givDosis(d2);
		assertFalse(outcome);
	}
	
	@Test
	public void testGivDosis_TC3() {
		pn = new PN(startDate, stopDate, testLM, 2);
		assertNotNull(pn);
		boolean outcome = pn.givDosis(d3);
		assertFalse(outcome);
	}

	@Test
	public void testGetAntalGangeGivet() {
		fail("Not yet implemented"); // TODO
	}

}
