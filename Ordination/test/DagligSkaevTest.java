package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.Patient;
import ordination.Dosis;

public class DagligSkaevTest {
	LocalDate startDen;
	LocalDate slutDen;
	Laegemiddel paracetamol;
	Patient pt;

	@Before
	public void setUp() throws Exception {
		startDen = LocalDate.of(2021, 3, 8);
		slutDen = LocalDate.of(2021, 3, 10);
		paracetamol = new Laegemiddel("Paracetamol", 5, 20, 30, "mg");
		pt = new Patient("22222222", "Flemming Frandsen", 92);
	}

	@Test
	public void testSamletDosis() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), paracetamol);
		assertNotNull(ds);
		DagligSkaev testDs = new DagligSkaev(startDen, slutDen, paracetamol);
		
		testDs.opretDosis(LocalTime.of(10, 00), 2);
		testDs.opretDosis(LocalTime.of(14, 30), 1);
		testDs.opretDosis(LocalTime.of(17, 00), 3);
		
		ds.opretDosis(LocalTime.of(10, 00), 2);
		ds.opretDosis(LocalTime.of(14, 30), 1);
		ds.opretDosis(LocalTime.of(17, 00), 3);
		System.out.println(ds.samletDosis());
		System.out.println(testDs.samletDosis());
		assertEquals(18, ds.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), paracetamol);
		assertNotNull(ds);
		ds.opretDosis(LocalTime.of(10, 00), 2);
		ds.opretDosis(LocalTime.of(14, 30), 1);
		ds.opretDosis(LocalTime.of(17, 00), 3);
		System.out.println(ds.doegnDosis());
		assertEquals(6, ds.doegnDosis(), 0.001);
	}

	@Test
	public void testOpretDosis() {
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), paracetamol);
		assertNotNull(ds);
        DagligSkaev testDs = new DagligSkaev(startDen, slutDen, paracetamol);
		ArrayList<Dosis> testDoser = new ArrayList<>();
		LocalTime testtime = LocalTime.of(07, 00);
		assertNotNull(testtime);		
	    testDs.opretDosis(testtime, 0);
	    testDoser.add(testDs.getDoser().get(0));	    
		ds.opretDosis(testtime, 0);
	    double antalIDs = ds.getDoser().get(0).getAntal();
	    double antalITest = testDs.getDoser().get(0).getAntal(); 
		assertEquals(0, antalIDs, 0.001);
		assertEquals(0, antalITest, 0.001);
		assertEquals(antalIDs, antalITest, 0.001);
		assertEquals(LocalTime.of(07, 00), testtime);
		assertTrue(ds.getDoser().contains(ds.getDoser().get(0)));
		assertEquals(ds.getDoser().size(), testDoser.size());
	}
	
	@Test
	public void testOpretDosis2() 
	{
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 3, 8), LocalDate.of(2021, 3, 10), paracetamol);
		assertNotNull(ds);
        DagligSkaev testDs = new DagligSkaev(startDen, slutDen, paracetamol);
        ArrayList<Dosis> testDoser = new ArrayList<>();
		LocalTime testtime = LocalTime.of(07, 00);
		testDs.opretDosis(testtime, 1);
	    testDoser.add(testDs.getDoser().get(0));
	    ds.opretDosis(testtime, 1);
	    double antalIDs = ds.getDoser().get(0).getAntal();
	    double antalITest = testDs.getDoser().get(0).getAntal();
	    assertEquals(LocalTime.of(07, 00), testtime);
	    assertEquals(1, antalIDs, 0001);
	    assertEquals(1, antalITest, 0001);
	    assertEquals(antalIDs, antalITest, 0.001);
	    assertTrue(ds.getDoser().contains(ds.getDoser().get(0)));
	    assertEquals(ds.getDoser().size(), testDoser.size());
	}

}
